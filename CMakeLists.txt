cmake_minimum_required(VERSION 2.8.3)
project(OptiTrack)

########################################################################
# comment these lines if you do not want ros to compile it for you!
find_package(catkin)
IF(catkin_FOUND)
    catkin_package(
            INCLUDE_DIRS include
            LIBRARIES ${PROJECT_NAME}
    )
ENDIF(catkin_FOUND)
########################################################################

include_directories(include ${catkin_INCLUDE_DIRS})

# building an example file
add_executable(OptiTrack_exampleCode src/exampleCode.cpp )
target_link_libraries(OptiTrack_exampleCode OptiTrack -lpthread )

# building the optiTrack library
add_library(${PROJECT_NAME} src/OptiTrack.cpp )

# compiling the libvrpn, if we have not done it yet
IF(NOT(EXISTS "${PROJECT_SOURCE_DIR}/thirdparty/vrpn"))
    execute_process(COMMAND sh ${PROJECT_SOURCE_DIR}/install_vrpn.sh ${PROJECT_SOURCE_DIR})
ENDIF(NOT(EXISTS "${PROJECT_SOURCE_DIR}/thirdparty/vrpn"))

target_link_libraries(OptiTrack vrpn )

